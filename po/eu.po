# Basque translation for scratch
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the scratch package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: scratch\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-01 17:45+0000\n"
"PO-Revision-Date: 2016-09-11 21:29+0000\n"
"Last-Translator: Thadah Denyse <juchuf@gmail.com>\n"
"Language-Team: Basque <eu@li.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-05-03 06:01+0000\n"
"X-Generator: Launchpad (build 18366)\n"

#: src/Application.vala:39
msgid "New Tab"
msgstr "Fitxa berria"

#: src/Application.vala:40 src/FolderManager/FileItem.vala:31
msgid "New Window"
msgstr "Leiho berria"

#: src/Application.vala:41
msgid "Print version info and exit"
msgstr ""

#: src/Application.vala:42
msgid "Set of plugins"
msgstr "Plugin-sorta"

#: src/Application.vala:42
#, fuzzy
msgid "plugin"
msgstr "Plugin-sorta"

#: src/Application.vala:43
msgid "[FILE…]"
msgstr ""

#: src/MainWindow.vala:140 src/MainWindow.vala:420 src/MainWindow.vala:445
msgid "Code"
msgstr ""

#: src/MainWindow.vala:202 src/Widgets/HeaderBar.vala:92
msgid "Find…"
msgstr "Bilatu..."

#: src/MainWindow.vala:207
msgid "Hide search bar"
msgstr "Ezkutatu bilaketa-barra"

#: src/MainWindow.vala:737 src/Services/Document.vala:487
msgid "All files"
msgstr "Fitxategi guztiak"

#: src/MainWindow.vala:741 src/Services/Document.vala:491
msgid "Text files"
msgstr "Testu-fitxategiak"

#: src/MainWindow.vala:745
msgid "Open some files"
msgstr "Fitxategi batzuk ireki"

#: src/MainWindow.vala:748
msgid "Open"
msgstr "Ireki"

#: src/MainWindow.vala:749 src/Dialogs/GlobalSearchDialog.vala:100
#: src/Dialogs/NewBranchDialog.vala:42 src/Services/Document.vala:394
#: src/Services/Document.vala:499 plugins/pastebin/pastebin_dialog.vala:327
#: plugins/pastebin/pastebin_dialog.vala:378
msgid "Cancel"
msgstr "Utzi"

#: src/MainWindow.vala:774
msgid "_Open"
msgstr "_Ireki"

#: src/MainWindow.vala:775
msgid "_Cancel"
msgstr "_Utzi"

#: src/Dialogs/GlobalSearchDialog.vala:60
#, c-format
msgid "Search for text in “%s”"
msgstr ""

#: src/Dialogs/GlobalSearchDialog.vala:61
msgid "The search term must be at least 3 characters long."
msgstr ""

#: src/Dialogs/GlobalSearchDialog.vala:74
msgid "Case sensitive:"
msgstr ""

#: src/Dialogs/GlobalSearchDialog.vala:83
msgid "Use regular expressions:"
msgstr ""

#: src/Dialogs/GlobalSearchDialog.vala:102
#, fuzzy
#| msgid "Search next"
msgid "Search"
msgstr "Hurrengoa bilatu"

#: src/Dialogs/NewBranchDialog.vala:43
#, fuzzy, c-format
#| msgid "Create a new instance"
msgid "Create a new branch of “%s/%s”"
msgstr "Sortu instantzia berri bat"

#. /TRANSLATORS "Git" is a proper name and must not be translated
#: src/Dialogs/NewBranchDialog.vala:48
msgid "The branch name must be unique and follow Git naming rules."
msgstr ""

#: src/Dialogs/NewBranchDialog.vala:56
#, fuzzy
msgid "Create Branch"
msgstr "Aldatu Scratch-en ezarpenak"

#: src/Dialogs/PreferencesDialog.vala:37 src/Widgets/HeaderBar.vala:161
msgid "Preferences"
msgstr "Hobespenak"

#: src/Dialogs/PreferencesDialog.vala:47
msgid ""
"Cutting or copying without an active selection will cut or copy the entire "
"current line"
msgstr ""

#: src/Dialogs/PreferencesDialog.vala:55
msgid "General"
msgstr "Orokorra"

#: src/Dialogs/PreferencesDialog.vala:56
msgid "Save files when changed:"
msgstr "Gorde fitxategiak aldatzean:"

#: src/Dialogs/PreferencesDialog.vala:58
msgid "Smart cut/copy lines:"
msgstr ""

#: src/Dialogs/PreferencesDialog.vala:61 src/Widgets/FormatBar.vala:44
msgid "Tabs"
msgstr "Fitxak"

#: src/Dialogs/PreferencesDialog.vala:62 src/Widgets/FormatBar.vala:145
msgid "Automatic indentation:"
msgstr "Koska automatikoa:"

#: src/Dialogs/PreferencesDialog.vala:64 src/Widgets/FormatBar.vala:139
msgid "Insert spaces instead of tabs:"
msgstr "Txertatu zuriuneak tabuladoreen ordez:"

#: src/Dialogs/PreferencesDialog.vala:66 src/Widgets/FormatBar.vala:142
msgid "Tab width:"
msgstr "Tabuladorearen zabalera:"

#: src/Dialogs/PreferencesDialog.vala:72
msgid "Behavior"
msgstr "Portaera"

#: src/Dialogs/PreferencesDialog.vala:73
msgid "Interface"
msgstr "Interfazea"

#: src/Dialogs/PreferencesDialog.vala:88
#: plugins/pastebin/pastebin_dialog.vala:434
msgid "Close"
msgstr "Itxi"

#: src/Dialogs/PreferencesDialog.vala:102
msgid "Extensions"
msgstr "Hedapenak"

#: src/Dialogs/PreferencesDialog.vala:111
msgid "Editor"
msgstr "Editorea"

#: src/Dialogs/PreferencesDialog.vala:113
msgid "Highlight matching brackets:"
msgstr "Nabarmendu parentesi pareak:"

#: src/Dialogs/PreferencesDialog.vala:116
msgid "Line wrap:"
msgstr "Lerro-doipena:"

#: src/Dialogs/PreferencesDialog.vala:119
#, fuzzy
#| msgid "Strip trailing whitespace"
msgid "Visible whitespace:"
msgstr "Garbitu amaierako zuriunea"

#: src/Dialogs/PreferencesDialog.vala:125
msgid "Show Mini Map:"
msgstr ""

#: src/Dialogs/PreferencesDialog.vala:128
msgid "Line width guide:"
msgstr "Lerro-zabaleraren gida:"

#: src/Dialogs/PreferencesDialog.vala:136
msgid "Font"
msgstr ""

#: src/Dialogs/PreferencesDialog.vala:138
msgid "Custom font:"
msgstr "Letra-tipo pertsonalizatua:"

#: src/Dialogs/RestoreConfirmationDialog.vala:31
msgid "Are You Sure You Want to Restore This File?"
msgstr ""

#: src/Dialogs/RestoreConfirmationDialog.vala:32
msgid "Restoring a file will undo all changes made since opening it"
msgstr ""

#: src/Dialogs/RestoreConfirmationDialog.vala:34
msgid "Don't Restore"
msgstr ""

#: src/Dialogs/RestoreConfirmationDialog.vala:36
msgid "Restore Anyway"
msgstr ""

#: src/FolderManager/FileItem.vala:52 src/FolderManager/FolderItem.vala:126
msgid "Other Application…"
msgstr ""

#: src/FolderManager/FileItem.vala:113 src/FolderManager/FolderItem.vala:166
#, fuzzy
msgid "Open In"
msgstr "Ireki"

#: src/FolderManager/FileItem.vala:116 src/FolderManager/FolderItem.vala:94
#, fuzzy
msgid "Other Actions"
msgstr "Beste formatuak"

#: src/FolderManager/FileItem.vala:119 src/FolderManager/FolderItem.vala:97
msgid "Rename"
msgstr ""

#: src/FolderManager/FileItem.vala:125 src/FolderManager/FolderItem.vala:103
#: src/FolderManager/ProjectFolderItem.vala:120
msgid "Move to Trash"
msgstr ""

#: src/FolderManager/FileView.vala:39
msgid "Folders"
msgstr "Karpetak"

#: src/FolderManager/FolderItem.vala:106
#, fuzzy
#| msgid "Open a folder"
msgid "Find in Folder…"
msgstr "Ireki karpeta"

#: src/FolderManager/FolderItem.vala:173
#, fuzzy
msgid "Folder"
msgstr "Karpetak"

#: src/FolderManager/FolderItem.vala:176
msgid "Empty File"
msgstr ""

#. scan all children
#. No need to show status when children shown
#: src/FolderManager/FolderItem.vala:183
#: src/FolderManager/ProjectFolderItem.vala:190
#: src/FolderManager/ProjectFolderItem.vala:196
#, fuzzy
msgid "New"
msgstr "Fitxa berria"

#: src/FolderManager/FolderItem.vala:344
msgid "untitled folder"
msgstr ""

#: src/FolderManager/FolderItem.vala:344
#, fuzzy
msgid "new file"
msgstr "Fitxategi berria"

#: src/FolderManager/ProjectFolderItem.vala:111
msgid "Close Folder"
msgstr "Itxi karpeta"

#: src/FolderManager/ProjectFolderItem.vala:116
#, fuzzy
#| msgid "Close Folder"
msgid "Close Other Folders"
msgstr "Itxi karpeta"

#: src/FolderManager/ProjectFolderItem.vala:127
#, fuzzy
#| msgid "Open a folder"
msgid "Find in Project…"
msgstr "Ireki karpeta"

#: src/FolderManager/ProjectFolderItem.vala:196
msgid "Modified"
msgstr ""

#: src/FolderManager/ProjectFolderItem.vala:234
#, c-format
msgid "Error while creating new branch: “%s”"
msgstr ""

#: src/FolderManager/ProjectFolderItem.vala:515
#, fuzzy
msgid "New Branch…"
msgstr "Aldatu Scratch-en ezarpenak"

#: src/FolderManager/ProjectFolderItem.vala:528
#, fuzzy
msgid "Branch"
msgstr "Aldatu Scratch-en ezarpenak"

#: src/Services/Document.vala:264
#, c-format
msgid "%s Is Not a Text File"
msgstr ""

#: src/Services/Document.vala:265
msgid "Code will not load this type of file."
msgstr ""

#: src/Services/Document.vala:267
msgid "Load Anyway"
msgstr ""

#: src/Services/Document.vala:288
#, c-format
msgid "Loading File \"%s\" Is Taking a Long Time"
msgstr ""

#: src/Services/Document.vala:289
msgid "Please wait while Code is loading the file."
msgstr ""

#: src/Services/Document.vala:291
#, fuzzy
msgid "Cancel Loading"
msgstr "Utzi"

#: src/Services/Document.vala:384
#, fuzzy, c-format
msgid "Save changes to \"%s\" before closing?"
msgstr "%s dokumentuaren aldaketak gorde irten aurretik?"

#: src/Services/Document.vala:385
#, fuzzy
msgid "If you don't save, changes will be permanently lost."
msgstr ""
"Ez baduzu gordetzen, azken 4 segundotako aldaketak betirako galduko dira."

#: src/Services/Document.vala:391
#, fuzzy
msgid "Close Without Saving"
msgstr "Itxi gorde gabe"

#: src/Services/Document.vala:395 src/Services/Document.vala:498
#: src/Services/Document.vala:730
msgid "Save"
msgstr "Gorde"

#: src/Services/Document.vala:495
msgid "Save File"
msgstr "Gorde fitxategia"

#: src/Services/Document.vala:572 src/Services/Document.vala:581
msgid "New Document"
msgstr "Dokumentu berria"

#. Show an error view which says "Hey, I cannot read that file!"
#: src/Services/Document.vala:704
#, c-format
msgid "File \"%s\" Cannot Be Read"
msgstr ""

#: src/Services/Document.vala:705
msgid "It may be corrupt or you don't have permission to read it."
msgstr ""

#: src/Services/Document.vala:718
#, c-format
msgid ""
"The location containing the file \"%s\" was unmounted. Do you want to save "
"somewhere else?"
msgstr ""

#: src/Services/Document.vala:721
msgid "Save As…"
msgstr "Gorde honela..."

#: src/Services/Document.vala:727
#, c-format
msgid "File \"%s\" was deleted. Do you want to save it anyway?"
msgstr ""

#: src/Services/Document.vala:744
#, c-format
msgid ""
"You cannot save changes to the file \"%s\". Do you want to save the changes "
"somewhere else?"
msgstr ""

#: src/Services/Document.vala:747
msgid "Save changes elsewhere"
msgstr "Gorde aldaketak beste nonbait"

#: src/Services/Document.vala:781
#, c-format
msgid ""
"File \"%s\" was modified by an external application. Do you want to load it "
"again or continue your editing?"
msgstr ""
"\"%s\" fitxategia kanpoko aplikazio batengatik aldatua izan da. Berriro "
"kargatu nahi duzu ala zure edizioa jarraitu?"

#: src/Services/Document.vala:784
msgid "Load"
msgstr "Kargatu"

#: src/Services/Document.vala:787
msgid "Continue"
msgstr "Jarraitu"

#: src/Services/TemplateManager.vala:192
msgid "Templates"
msgstr "Txantiloiak"

#: src/Widgets/ChooseProjectButton.vala:20
#, fuzzy
#| msgid "Project templates"
msgid "No Project Selected"
msgstr "Proiektuaren txantiloiak"

#: src/Widgets/ChooseProjectButton.vala:38
#: src/Widgets/ChooseProjectButton.vala:100
#: src/Widgets/ChooseProjectButton.vala:138
#, c-format
msgid "Active Git project: %s"
msgstr ""

#: src/Widgets/ChooseProjectButton.vala:53
msgid "Filter projects"
msgstr ""

#: src/Widgets/DocumentView.vala:126
#, fuzzy, c-format
msgid "Text file from %s:%d"
msgstr "Testu-fitxategiak"

#: src/Widgets/FormatBar.vala:48
msgid "Syntax Highlighting"
msgstr "Sintaxiaren nabarmentzea"

#: src/Widgets/FormatBar.vala:56
#, fuzzy
msgid "Line number"
msgstr "Erakutsi lerro zenbakiak:"

#: src/Widgets/FormatBar.vala:83
msgid "Filter languages"
msgstr ""

#: src/Widgets/FormatBar.vala:107
msgid "Plain Text"
msgstr ""

#: src/Widgets/FormatBar.vala:189
#, c-format
msgid "%d Space"
msgid_plural "%d Spaces"
msgstr[0] ""
msgstr[1] ""

#: src/Widgets/FormatBar.vala:191
#, fuzzy, c-format
msgid "%d Tab"
msgid_plural "%d Tabs"
msgstr[0] "Fitxa berria"
msgstr[1] "Fitxa berria"

#: src/Widgets/FormatBar.vala:207
msgid "Go To Line:"
msgstr "Joan lerrora:"

#: src/Widgets/HeaderBar.vala:59
msgid "Open a file"
msgstr "Ireki fitxategi bat"

#: src/Widgets/HeaderBar.vala:64
msgid "Project templates"
msgstr "Proiektuaren txantiloiak"

#: src/Widgets/HeaderBar.vala:70
msgid "Save this file"
msgstr "Gorde fitxategi hau"

#: src/Widgets/HeaderBar.vala:77
msgid "Save this file with a different name"
msgstr "Gorde fitxategi hau izen desberdin batekin"

#: src/Widgets/HeaderBar.vala:84
msgid "Restore this file"
msgstr "Leheneratu fitxategi hau"

#: src/Widgets/HeaderBar.vala:99
msgid "Share"
msgstr "Partekatu"

#: src/Widgets/HeaderBar.vala:106
#, fuzzy
msgid "Zoom Out"
msgstr "Zoom-a"

#: src/Widgets/HeaderBar.vala:113
msgid "Zoom 1:1"
msgstr "Zoom-a 1:1"

#: src/Widgets/HeaderBar.vala:120
#, fuzzy
msgid "Zoom In"
msgstr "Zoom-a"

#: src/Widgets/HeaderBar.vala:151
#, fuzzy
msgid "Toggle Sidebar"
msgstr "Albo-barra"

#: src/Widgets/HeaderBar.vala:182
msgid "Menu"
msgstr ""

#: src/Widgets/Sidebar.vala:42
#, fuzzy
#| msgid "Open a folder"
msgid "Open Project Folder…"
msgstr "Ireki karpeta"

#: src/Widgets/Sidebar.vala:45
msgid "Collapse All"
msgstr ""

#: src/Widgets/Sidebar.vala:49
msgid "Alphabetize"
msgstr ""

#: src/Widgets/Sidebar.vala:61
msgid "Manage project folders"
msgstr ""

#: src/Widgets/SearchBar.vala:67
msgid "Find"
msgstr "Bilatu"

#. We don't want to flicker back to no results while we're still searching but we have previous results
#: src/Widgets/SearchBar.vala:69 src/Widgets/SearchBar.vala:587
#: src/Widgets/SearchBar.vala:590
msgid "no results"
msgstr ""

#: src/Widgets/SearchBar.vala:82
msgid "Search next"
msgstr "Hurrengoa bilatu"

#: src/Widgets/SearchBar.vala:93
msgid "Search previous"
msgstr "Aurrekoa bilatu"

#: src/Widgets/SearchBar.vala:98
msgid "Cyclic Search"
msgstr "Bilaketa ziklikoa"

#. Need to SYNC_CREATE so tooltip present before toggled
#. tooltip_val.set_string () does not work (?)
#: src/Widgets/SearchBar.vala:111
msgid "Case Sensitive"
msgstr ""

#: src/Widgets/SearchBar.vala:111
msgid "Case Insensitive"
msgstr ""

#: src/Widgets/SearchBar.vala:119
msgid "Use regular expressions"
msgstr ""

#: src/Widgets/SearchBar.vala:140
msgid "Replace With"
msgstr "Ordezkatu honekin"

#: src/Widgets/SearchBar.vala:143
msgid "Replace"
msgstr "Ordezkatu"

#: src/Widgets/SearchBar.vala:146
msgid "Replace all"
msgstr "Ordeztu denak"

#: src/Widgets/SearchBar.vala:586
#, c-format
msgid "%d of %d"
msgstr ""

#: src/Widgets/SourceView.vala:514
msgid "Sort Selected Lines"
msgstr ""

#: src/Widgets/SourceView.vala:527
msgid "Toggle Comment"
msgstr ""

#: src/Widgets/WelcomeView.vala:26
msgid "No Files Open"
msgstr "Ez dago fitxategirik irekita"

#: src/Widgets/WelcomeView.vala:27
msgid "Open a file to begin editing."
msgstr "Ireki fitxategi bat editatzen hasteko."

#: src/Widgets/WelcomeView.vala:32
#, fuzzy
msgid "New File"
msgstr "/Fitxategi berria"

#: src/Widgets/WelcomeView.vala:32
msgid "Create a new empty file."
msgstr "Sortu fitxategi huts berri bat."

#: src/Widgets/WelcomeView.vala:33
#, fuzzy
msgid "Open File"
msgstr "Ireki fitxategia"

#: src/Widgets/WelcomeView.vala:33
msgid "Open a saved file."
msgstr "Ireki gordetako fitxategi bat."

#: src/Widgets/WelcomeView.vala:34
#, fuzzy
msgid "Open Folder"
msgstr "Ireki karpeta"

#: src/Widgets/WelcomeView.vala:34
msgid "Add a project folder to the sidebar."
msgstr ""

#: plugins/pastebin/pastebin_dialog.vala:248
#: plugins/pastebin/pastebin_dialog.vala:328 plugins/pastebin/pastebin.vala:96
msgid "Upload to Pastebin"
msgstr "Igo Pastebin-era"

#: plugins/pastebin/pastebin_dialog.vala:257
msgid "Name:"
msgstr "Izena:"

#: plugins/pastebin/pastebin_dialog.vala:261
#, fuzzy
msgid "Format:"
msgstr "Formatua: "

#: plugins/pastebin/pastebin_dialog.vala:268
msgid "Choose different format"
msgstr ""

#: plugins/pastebin/pastebin_dialog.vala:290
#, fuzzy
#| msgid "Expiry time:"
msgid "Expiration:"
msgstr "Epemuga:"

#: plugins/pastebin/pastebin_dialog.vala:297
msgid "Keep this paste private"
msgstr "Mantendu eduki hau pribatu"

#: plugins/pastebin/pastebin_dialog.vala:354
msgid "Available Formats"
msgstr ""

#: plugins/pastebin/pastebin_dialog.vala:380
#, fuzzy
#| msgid "Select font:"
msgid "Select Format"
msgstr "Aukeratu letra-tipoa:"

#: plugins/pastebin/pastebin_dialog.vala:467
msgid "Never"
msgstr "Inoiz ez"

#: plugins/pastebin/pastebin_dialog.vala:468
msgid "Ten minutes"
msgstr "Hamar minutu"

#: plugins/pastebin/pastebin_dialog.vala:469
msgid "One hour"
msgstr "Ordu bat"

#: plugins/pastebin/pastebin_dialog.vala:470
msgid "One day"
msgstr "Egun bat"

#: plugins/pastebin/pastebin_dialog.vala:471
msgid "One month"
msgstr "Hilabete bat"

#. Remove fake fields created by the vala parser.
#: plugins/outline/C/CtagsSymbolOutline.vala:31
#: plugins/outline/C/CtagsSymbolOutline.vala:71
#: plugins/outline/Vala/ValaSymbolOutline.vala:38
#: plugins/outline/Vala/ValaSymbolOutline.vala:120
#: plugins/outline/OutlinePane.vala:27
msgid "Symbols"
msgstr "Ikurrak"

#: plugins/outline/OutlinePlugin.vala:34
#, fuzzy
#| msgid "Symbols"
msgid "No Symbols Found"
msgstr "Ikurrak"

#: plugins/terminal/terminal.vala:171 plugins/terminal/terminal.vala:187
msgid "Show Terminal"
msgstr "Erakutsi terminala"

#: plugins/terminal/terminal.vala:174
msgid "Hide Terminal"
msgstr "Ezkutatu terminala"

#: plugins/terminal/terminal.vala:184
msgid "Terminal"
msgstr "Terminala"

#. Popup menu
#. COPY
#: plugins/terminal/terminal.vala:245
msgid "Copy"
msgstr "Kopiatu"

#. PASTE
#: plugins/terminal/terminal.vala:252
msgid "Paste"
msgstr "Itsatsi"

#: plugins/spell/spell.vala:73
msgid "No Suitable Dictionaries Were Found"
msgstr ""

#: plugins/spell/spell.vala:74
msgid "Please install at least one [aspell] dictionary."
msgstr ""

#: plugins/word-completion/plugin.vala:162
#, c-format
msgid "%s - Word Completion"
msgstr "%s - Hitzak osatu"

#, fuzzy
#~| msgid "Search next"
#~ msgid "Search for:"
#~ msgstr "Hurrengoa bilatu"

#, fuzzy
#~ msgid "Zeitgeist Datasource for Code"
#~ msgstr "Scratch-entzako Zeitgeist datu-iturburua"

#~ msgid "Web Preview"
#~ msgstr "Web aurreikuspena"

#~ msgid "Show Preview"
#~ msgstr "Erakutsi aurreikuspena"

#~ msgid "Hide Preview"
#~ msgstr "Ezkutatu aurreikuspena"

#, fuzzy
#~ msgid "Share via Pastebin"
#~ msgstr "Partekatu PasteBin erabiliz"

#, fuzzy
#~ msgid "Others…"
#~ msgstr "Besteak..."

#~ msgid "Upload"
#~ msgstr "Kargatu"

#~ msgid "Other formats"
#~ msgstr "Beste formatuak"

#~ msgid "Add New View"
#~ msgstr "Gehitu bista berria"

#~ msgid "Remove Current View"
#~ msgstr "Kendu uneko bista"

#, fuzzy
#~ msgid "new file %d"
#~ msgstr "Fitxategi berria"

#~ msgid "Could not load icon theme: %s\n"
#~ msgstr "Ezin izan da ikonoen gaia kargatu: %s\n"

#~ msgid "Highlight current line:"
#~ msgstr "Nabarmendu uneko lerroa:"

#~ msgid "New file"
#~ msgstr "Fitxategi berria"

#~ msgid "New file from clipboard"
#~ msgstr "Fitxategi berria arbeletik"

#~ msgid "Create a new file from the contents of your clipboard."
#~ msgstr "Fitxategi berri bat sortu arbeleko edukitik."

#~ msgid "Open With…"
#~ msgstr "Honekin zabaldu..."

#~ msgid "Terminal on Right"
#~ msgstr "Terminala eskuinean"

#~ msgid "Terminal on Bottom"
#~ msgstr "Terminala azpian"

#~ msgid "Color scheme:"
#~ msgstr "Kolore-eskema:"

#~ msgid "Edit text files"
#~ msgstr "Editatu testu-fitxategiak"

#~ msgid "Text Editor"
#~ msgstr "Testu-editorea"

#~ msgid "About Scratch"
#~ msgstr "Scratcheri Buruz"

#~ msgid "Trash"
#~ msgstr "Zakarrontzia"

#~ msgid "Go to line…"
#~ msgstr "Joan lerrora..."

#~ msgid "Quit"
#~ msgstr "Irten"

#~ msgid "Reopen closed document"
#~ msgstr "Berrireki itxitako dokumentua"

#~ msgid "Open last closed document in a new tab"
#~ msgstr "Ireki azkena itxitako dokumentua fitxa berrian"

#~ msgid "Add New Tab"
#~ msgstr "Gehitu fitxa berria"

#~ msgid "Add a new tab"
#~ msgstr "Gehitu fitxa berri bat"

#~ msgid "Add a new view"
#~ msgstr "Gehitu bista berri bat"

#~ msgid "Remove this view"
#~ msgstr "Kendu bista hau"

#~ msgid "Undo"
#~ msgstr "Desegin"

#~ msgid "Undo the last action"
#~ msgstr "Desegin azken ekintza"

#~ msgid "Redo"
#~ msgstr "Berregin"

#~ msgid "Redo the last undone action"
#~ msgstr "Berregin desegindako azken ekintza"

#~ msgid "Revert"
#~ msgstr "Leheneratu"

#~ msgid "Duplicate selected strings"
#~ msgstr "Bikoiztu hautatutako kateak"

#~ msgid "Clipboard"
#~ msgstr "Arbela"

#~ msgid "New file from Clipboard"
#~ msgstr "Fitxategi berria arbeletik"

#~ msgid "Next Tab"
#~ msgstr "Hurrengo fitxa"

#~ msgid "Previous Tab"
#~ msgstr "Aurreko fitxa"

#~ msgid "Fullscreen"
#~ msgstr "Pantaila osoa"

#~ msgid "translator-credits"
#~ msgstr ""
#~ "Launchpad Contributions:\n"
#~ "  Asier Iturralde Sarasola https://launchpad.net/~asier-iturralde\n"
#~ "  Eneko Sarasola https://launchpad.net/~eneko-sarasola\n"
#~ "  Félix Brezo https://launchpad.net/~febrezo\n"
#~ "  Ibai Oihanguren Sala https://launchpad.net/~ibai-oihanguren\n"
#~ "  KombuchaMushroom https://launchpad.net/~enekosar\n"
#~ "  Mario Guerriero https://launchpad.net/~mefrio-g\n"
#~ "  Thadah Denyse https://launchpad.net/~juchuf"

#~ msgid "Wait while restoring last session..."
#~ msgstr "Itxaron azken saioa kargatu bitartean..."

#~ msgid "Normal Text"
#~ msgstr "Testu arrunta"

#~ msgid "When Scratch starts:"
#~ msgstr "Scratch hasten denean:"

#~ msgid "Show welcome screen"
#~ msgstr "Erakutsi ongietorri pantaila"

#~ msgid "Show last open tabs"
#~ msgstr "Erakutsi irekitako azken fitxak"

#~ msgid "A terminal in your text editor"
#~ msgstr "Terminal bat testu-editorean"

#~ msgid "Strip trailing whitespace on save"
#~ msgstr "Garbitu amaierako zuriunea gordetzerakoan"

#~ msgid "Highlight Selected Words"
#~ msgstr "Aukeratutako hitzak azpimarratu"

#~ msgid "Highlights all occurrences of words that are selected"
#~ msgstr "Aukeratuta dauden hitzen agerpen guztiak azpimarratzen ditu"

#~ msgid "Brackets Completion"
#~ msgstr "Parentesiak osatu"

#~ msgid "Complete brackets while typing"
#~ msgstr "Osatu parentesiak idatzi ahala"

#~ msgid "Source Tree"
#~ msgstr "Iturburu-zuhaitza"

#~ msgid "Have a look at your sources organized in a nice tree"
#~ msgstr "Ikusi iturburua zuhaitz gisa antolaturik"

#~ msgid "Loading..."
#~ msgstr "Kargatzen..."

#~ msgid "Bookmark"
#~ msgstr "Laster-marka"

#~ msgid "Files"
#~ msgstr "Fitxategiak"

#~ msgid "Project"
#~ msgstr "Proiektua"

#~ msgid "Bookmarks"
#~ msgstr "Laster-markak"

#~ msgid "Vim Emulation"
#~ msgstr "Vim emulazioa"

#~ msgid "Use Vim commands in Scratch"
#~ msgstr "Erabili Vim aginduak Scratch-en"

#~ msgid "Clipboard History"
#~ msgstr "Arbelaren historiala"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Delete"
#~ msgstr "Ezabatu"

#~ msgid "Folder Manager"
#~ msgstr "Karpeta-kudeatzailea"

#~ msgid "Basic folder manager with file browsing"
#~ msgstr "Oinarrizko karpeta-kudeatzailea fitxategiak arakatzeko aukerarekin"

#~ msgid "Go to parent"
#~ msgstr "Joan gurasora"

#~ msgid "Add file"
#~ msgstr "Gehitu fitxategia"

#~ msgid "Remove file"
#~ msgstr "Kendu fitxategia"

#~ msgid "File Manager"
#~ msgstr "Fitxategi-kudeatzailea"

#~ msgid "Spell Checker"
#~ msgstr "Ortografia-egiaztatzailea"

#~ msgid "Checks the spelling of your documents"
#~ msgstr "Dokumentuen ortografia egiaztatzen du"

#~ msgid "Browser Preview"
#~ msgstr "Nabigatzailearen aurrebista"

#~ msgid "Get a preview your work in a web page"
#~ msgstr "Aurreikusi web orrietan egiten dituzun aldaketak"

#~ msgid "Words Completion"
#~ msgstr "Hitzak osatu"

#~ msgid "Show a completion dialog with most used words from your files"
#~ msgstr ""
#~ "Hitzak osatzeko laguntza-leiho bat erakutsi, fitxategietan gehien "
#~ "erabilitako hitzekin"

#~ msgid "Pastebin"
#~ msgstr "Pastebin"

#~ msgid "Share files with pastebin service"
#~ msgstr "Partekatu fitxategiak Pastebin zerbitzuarekin"

#~ msgid "Share your files with Contractor"
#~ msgstr "Partekatu fitxategiak Contractor erabiliz"

#~ msgid "Open With"
#~ msgstr "Ireki honekin"

#~ msgid "Open files you are editing with another application"
#~ msgstr "Ireki editatzen ari zaren fitxategiak beste aplikazio batekin"

#~ msgid "Detect Indent"
#~ msgstr "Identazioa detektatu"

#~ msgid "Changes to this file haven't been saved."
#~ msgstr "Fitxategi honen aldaketak ez dira gorde."

#~ msgid "Do you want to save changes before closing this file?"
#~ msgstr "Aldaketak gorde nahi dituzu fitxategi hau itxi aurretik?"

#~ msgid "Find..."
#~ msgstr "Bilatu..."

#~ msgid "Go to line..."
#~ msgstr "Joan lerrora..."

#~ msgid "New document"
#~ msgstr "Dokumentu berria"

#~ msgid "Save the current file with a different name"
#~ msgstr "Gorde uneko fitxategia beste izen batekin"

#~ msgid "Save as"
#~ msgstr "Gorde honela"

#~ msgid "Save the current file"
#~ msgstr "Gorde uneko fitxategia"

#~ msgid "Normal text"
#~ msgstr "Testu normala"

#~ msgid "Status Bar"
#~ msgstr "Egoera-barra"

#~ msgid "Editor:"
#~ msgstr "Editorea:"

#~ msgid "Tabs:"
#~ msgstr "Fitxak:"

#~ msgid "Margin width:"
#~ msgstr "Marjinaren zabalera:"

#~ msgid "Save unsaved changes to file before closing?"
#~ msgstr "Gorde azken aldaketak itxi aurretik?"

#~ msgid "Argument for the set of plugins"
#~ msgstr "Plugin-sortaren argumentuak"

#~ msgid "Create a new document in a new tab"
#~ msgstr "Sortu dokumentu berri bat fitxa berri batean"

#~ msgid "Previous Search"
#~ msgstr "Aurreko bilaketa"

#~ msgid "Next Search"
#~ msgstr "Hurrengo bilaketa"

#~ msgid "Create a new document from a template"
#~ msgstr "Sortu dokumentu berri bat, txantiloi bat erabiliz"

#~ msgid "Choose the new location"
#~ msgstr "Aukeratu kokapen berria"

#~ msgid "Bottom Panel"
#~ msgstr "Beheko panela"

#~ msgid "No files are open."
#~ msgstr "Ez dago fitxategirik irekita."

#~ msgid "Context View"
#~ msgstr "Testuinguru bista"

#~ msgid "General:"
#~ msgstr "Orokorra:"

#~ msgid "Show margin on right:"
#~ msgstr "Erakutsi marjina eskuinaldean:"

#~ msgid "Draw spaces:"
#~ msgstr "Marraztu zuriuneak:"

#~ msgid "Do you want to reload it?"
#~ msgstr "Birkargatu nahi duzu?"

#~ msgid "Do you want to create it again?"
#~ msgstr "Berriz sortu nahi duzu?"

#~ msgid "You can't save changes to:"
#~ msgstr "Ezin dituzu hemen aldaketak gorde:"

#~ msgid ""
#~ "Do you want to save the changes to this file in a different location?"
#~ msgstr ""
#~ "Fitxategi honi egindako aldaketak beste kokapen batean gorde nahi dituzu?"

#~ msgid "There are unsaved changes in Scratch!"
#~ msgstr "Gorde gabeko aldaketak daude Scratch-en!"

#~ msgid "There are unsaved changes!"
#~ msgstr "Gorde gabeko aldaketak daude!"

#~ msgid "Save unsaved changes to file %s before closing?"
#~ msgstr "Gorde gabeko aldaketak gorde %s(e)n itxi aurretik?"

#~ msgid "Font and Color Scheme:"
#~ msgstr "Letra-tipoa eta Koloreak"

#~ msgid "The file %s was modified."
#~ msgstr "%s fitxategia aldatua izan da."

#~ msgid "The file %s was deleted."
#~ msgstr "%s fitxategia ezabatua izan da."
